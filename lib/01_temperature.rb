def ftoc(fahtemp)
  (fahtemp.to_f - 32) * (5.to_f / 9.to_f).to_f
end

def ctof(celtemp)
  (celtemp.to_f * (9.to_f / 5.to_f)).to_f + 32
end

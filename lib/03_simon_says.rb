def echo(word_to_echo)
  word_to_echo
end

def shout(words_to_shout)
  words_to_shout.upcase
end

def repeat(words_to_repeat, times_to_repeat=nil)
  array_of_words = []
  if times_to_repeat.nil?
    return [words_to_repeat, words_to_repeat].join(' ')
  else
    times_to_repeat.times do
      array_of_words << words_to_repeat
    end
  end
  array_of_words.join(' ')
end

def start_of_word(word, position)
  word[0..(position - 1)]
end

def first_word(string_of_words)
  string_of_words.split(' ').first
end

def titleize(title_to_fix)
  little_words = '%w{and the over}'
  array_of_words = title_to_fix.split(' ')
  fixed_title = array_of_words.map do |word|
    little_words.include?(word) ? word : word.capitalize
  end.join(' ')
  fixed_title[0].upcase + fixed_title[1..-1]
end

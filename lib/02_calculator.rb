def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array_of_numbers)
  array_of_numbers.inject(0, :+)
end

def multiply(*numbers)
  numbers.inject(1, :*)
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  num.zero? ? 1 : num * factorial(num - 1)
end

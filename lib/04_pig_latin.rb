VOWELS = '%w{a e i o u}'
def translate(english_to_pig_latin)
  words = english_to_pig_latin.split(' ')
  words_in_pig_latin = []
  words.each { |word| words_in_pig_latin << translate_word(word) }
  words_in_pig_latin.join(' ')
end

def translate_word(word)
  words_parts = find_word_parts(word)
  start_with_vowel?(word) ? word + 'ay' : words_parts[1] + words_parts[0] + 'ay'
end

def find_word_parts(word)
  if three_consonants?(word)
    [word[0..2], word[3..-1]]
  elsif two_consonants?(word)
    [word[0..1], word[2..-1]]
  else
    [word[0], word[1..-1]]
  end
end

def start_with_vowel?(word)
  VOWELS.include?(word[0])
end

def three_consonants?(word)
  word[0..2].chars.all? do |char|
    VOWELS.include?(char) == false || word[0..2] == 'squ'
  end
end

def two_consonants?(word)
  word[0..1].chars.all? do |char|
    VOWELS.include?(char) == false || word[0..1] == 'qu'
  end
end
